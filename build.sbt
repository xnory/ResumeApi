name := "ResumeAPI"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val akkaV = "2.3.6"
  val sprayV = "1.3.2"
  Seq(
    "io.spray"            %%  "spray-can"     % sprayV,
    "io.spray"            %%  "spray-client"  % sprayV,
    "io.spray"            %%  "spray-routing" % sprayV,
    "io.spray"            %%  "spray-testkit" % sprayV  % "test",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
    "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test",
    "ch.qos.logback"      % "logback-classic" % "1.0.13",
    "com.typesafe.akka"   %%  "akka-slf4j"    % akkaV
  )
}

libraryDependencies += "com.typesafe.akka" %% "akka-remote" % "2.4.16"
libraryDependencies += "org.json4s" %% "json4s-native" % "3.2.10"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.2.10"
libraryDependencies += "io.spray" %%  "spray-json" % "1.3.1"
libraryDependencies += "joda-time" % "joda-time" % "2.9.4"

libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-nop" % "1.6.4"
)

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick"     % "3.1.1",
  "com.chuusai"        %% "shapeless" % "2.3.1",
  "io.underscore"      %% "slickless" % "0.3.2"
)

libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.0"
)

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.18"

libraryDependencies += "org.ddahl" % "rscala_2.11" % "1.0.13"
libraryDependencies += "com.itextpdf" % "itextpdf" % "5.5.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http-core" % "10.0.3",
  "com.typesafe.akka" %% "akka-http" % "10.0.3",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.3",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.3",
  "com.typesafe.akka" %% "akka-http-jackson" % "10.0.3",
  "com.typesafe.akka" %% "akka-http-xml" % "10.0.3"
)
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"