package com.jn

/**
  * Created by jerameynormand on 1/16/18.
  */
import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.util.Timeout
import spray.can.Http
import com.jn._

import scala.concurrent.duration._
import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import spray.can.Http
import spray.io.ServerSSLEngineProvider

object Boot extends App {
  implicit val system = ActorSystem("on-spray-can")

  implicit val timeout = Timeout(900.seconds)
  // the handler actor replies to incoming HttpRequests
  val handler = system.actorOf(Props[RouterActor], name = "handler")

  IO(Http) ! Http.Bind(handler, interface = "0.0.0.0", port = 8000)
}
