package com.jn

/**
  * Created by jerameynormand on 1/16/18.
  */

import akka.actor.{Actor, _}
import akka.util.Timeout
import java.util.Date
import spray.can.Http
import scala.concurrent.duration._
import spray.http._
import spray.http.HttpMethods._
//import com.jn.ResponseContainer

class RouterActor extends Actor {
  implicit val system: ActorSystem = ActorSystem()
  implicit val timeout: Timeout = akka.util.Timeout.apply(2.hours)

  def rightNow(): String = {
    val myDate = new Date(System.currentTimeMillis())
    myDate.toString
  }

  def receive = {
    case _: Http.Connected => sender ! Http.Register(self)

    case r@HttpRequest(GET, Uri.Path("/givemetheloot"), _, _, _) => {
      //println("in the isUp service, printing the headers: ")
      //r.headers.foreach(println)
      //println("priting the data: ")
      //println(r.entity.data.asString)
      println("At ", rightNow(), ":: the request received was : ", r)

      val queryMap = r.uri.query.toMap
      println("At ", rightNow(), ", They are asking for my: ", queryMap)

      val client = sender

      val question = queryMap.get("q").map(x=>{
        x match{
          case "Ping" =>{
            //val client = sender
            val res = "OK"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Name" =>{
            val res = "Jeramey Normand"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity =  res)
          }
          case "Puzzle" =>{//TODO: not done yet!!!!!!!!!!
//            val puzzleChallenge = queryMap.get("d").map(y=>{
//              val res = puzzleSolver(y)
//              (client ! HttpResponse(status = 200, entity = res))
//            })
            val puzzle = queryMap.get("d").map(_.toString)
            println("\n\n\n --PUZZLE TIME-- \n\n\n")
            println(puzzle)

            val res = " ABCD\nA=>>>\nB<=<<\nC<>=>\nD<><="
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Source" =>{//git repo of project
            val res = """https://gitlab.com/xnory/ResumeApi.git"""
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Degree" =>{
            val res = "Bachelor of Science in Computer Science, Union College, Schenectady, NY 11222"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Status" =>{
            val res = "Yes, I can provide proof of my eligibility to work in the United States of America."
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Phone" =>{
            val res = "9788158548"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Years" => {
            val res = "1.5 years"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Referrer" => {
            val res = "DV, CS"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Resume" => {
            val res = """http://jerameynormand.com/data/brt_cover_and_resume.pdf"""
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Position" => {
            val res = "Software Engineer"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case "Email Address" => {
            val res = "jerameynormand91@gmail.com"
            println("At ", rightNow(), ", I responded with: ", res)
            client ! HttpResponse(status = 200, entity = res)
          }
          case _ => {
            client ! HttpResponse(status = 400, entity = "No valid service found!")
          }
        }
      })
    }
  }
}


//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Ping&d=Please+return+OK+so+that+I+know+your+service+works.,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Ping, d -> Please return OK so that I know your service works.))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Puzzle&d=Please+solve+this+puzzle:%0A+ABCD%0AA%3D---%0AB%3E---%0AC%3C---%0AD-%3E--%0A,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Puzzle, d -> Please solve this puzzle:
//ABCD
//A=---
//B>---
//C<---
//D->--
//))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Source&d=Please+provide+a+URL+where+we+can+download+the+source+code+of+your+resume+submission+web+service.,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Source, d -> Please provide a URL where we can download the source code of your resume submission web service.))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Degree&d=Please+list+your+relevant+university+degree(s).,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Degree, d -> Please list your relevant university degree(s).))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Status&d=Can+you+provide+proof+of+eligibility+to+work+in+the+US?,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Status, d -> Can you provide proof of eligibility to work in the US?))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Phone&d=Please+provide+a+phone+number+we+can+use+to+reach+you.,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Phone, d -> Please provide a phone number we can use to reach you.))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Years&d=How+many+years+of+software+development+experience+do+you+have?,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))

//(the query map::>>>>> ,Map(q -> Years, d -> How many years of software development experience do you have?))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Referrer&d=How+did+you+hear+about+this+position?,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Referrer, d -> How did you hear about this position?))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Resume&d=Please+provide+a+URL+where+we+can+download+your+resume+and+cover+letter.,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))

//(the query map::>>>>> ,Map(q -> Resume, d -> Please provide a URL where we can download your resume and cover letter.))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Position&d=Which+position+are+you+applying+for?,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Position, d -> Which position are you applying for?))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Name&d=What+is+your+full+name?,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Name, d -> What is your full name?))
//(the request: ,HttpRequest(GET,http://jerameynormand.com:8000/isUp?q=Email+Address&d=What+is+your+email+address?,List(Host: jerameynormand.com:8000),Empty,HTTP/1.0))
//(the query map::>>>>> ,Map(q -> Email Address, d -> What is your email address?))
